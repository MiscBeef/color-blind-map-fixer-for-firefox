// Obligatory Functions
function onCreated() { }
function onRemoved() { }
function onError(error) { console.log(`Error: ${error}`); }

// Context Menu Item Entries
browser.menus.create({
	id: "process-categorical", // This is labeled categorical, because in theory there may be a continuous option in the future
	title: browser.i18n.getMessage("menuItemCategoricalData"),
	contexts: ["image"]
}, onCreated);

browser.menus.create({
	id: "restore-image",
	title: browser.i18n.getMessage("menuItemRestoreImage"),
	contexts: ["image"]
}, onCreated);

// Context Menu Item Handling. Calls relevant javascript functions embedded into pages by cbmf.js
browser.menus.onClicked.addListener((info, tab) => {
	switch (info.menuItemId) {
		case "process-categorical":
			browser.tabs.executeScript(tab.id, {
			frameId: info.frameId,
			code: `
				cbmfProcessImage(browser.menus.getTargetElement(${info.targetElementId}));
			`,
			});
		break;
		case "restore-image":
			browser.tabs.executeScript(tab.id, {
			frameId: info.frameId,
			code: `
				cbmfRestoreImage(browser.menus.getTargetElement(${info.targetElementId}));
			`,
			});
		break;
	}
});
