// Replaces the most common 14 colors in image (not counting white, light gray, or black) with 14 colors from a color-blind friendly color palette 
function cbmfProcessImage(image_to_process) {
	// Put image into canvas
	var new_canvas = document.createElement("canvas");
	if (!image_to_process.hasAttribute('cbmf-original-url')) { // original url tracking for cbmfRestoreImage
		image_to_process.setAttribute('cbmf-original-url', image_to_process.src);
	}
	new_canvas.width = image_to_process.width;
	new_canvas.height = image_to_process.height;
	new_context = new_canvas.getContext('2d');
	new_context.imageSmoothingEnabled = false;
	new_context.webkitImageSmoothingEnabled = false;
	new_context.mozImageSmoothingEnabled = false;
	new_context.drawImage(image_to_process, 0, 0, image_to_process.width, image_to_process.height); // need to specify width and height to avoid weird intermittent scaling issue
	
	// Get pixel data from image
	var data;
	try {
		data = new_context.getImageData(0, 0, image_to_process.width, image_to_process.height).data;
	}
	catch(err) {
		alert("Color-Blind Map Fixer caught error:\n" + err.message); // CORS lol
	}
	
	// Count colors in image to find and sort by the most common colors found in the image 
	var swap_colors = {};
	for (var i = 0; i < data.length; i+=4) {
		if (data[i] != data[i+1] || data[i+1] != data[i+2]) {
			var rgb = data[i] + " " + data[i+1] + " " + data[i+2];
			if (rgb in swap_colors) {
				swap_colors[rgb]++;
			} else {
				swap_colors[rgb] = 1;
			}
		}
	}
	sorted = []
	for (var key in swap_colors) {
		sorted[sorted.length] = [key,swap_colors[key]];
	}
	sorted = sorted.sort(
		function(a,b) {
			return b[1]-a[1];
		}
	);
	
	// Set up standard color palette
	var new_color_map = {};	
	var palette_colors = ["#B66DFF", "#B6DBFF", "#920000", "#DB6D00", "#FFFF6D", "#006DDB", "#FFB6DB", "#004949", "#6DB6FF", "#FF6DB6", "#924900",  "#24FF24", "#009292", "#490092"];

	// Assign standard color palette to top colors, but also merge very similar colors along the way
	var pc = 0;
	for (i = 0; i < sorted.length && i < 1000; i++) {
		for (var key in new_color_map) { // look for preexisting similar colors
			if (cbmfSimilarColors(key, sorted[i][0])) {
				new_color_map[sorted[i][0]] = new_color_map[key];
				break;
			}
		}		
		if (!(sorted[i][0] in new_color_map) && pc < 14) { // otherwise just iterate through next standard color if any standard colors remain
			new_color_map[sorted[i][0]] = palette_colors[pc];
			pc++;
		}
	}
	
	// Color in the image with palette colors
	var x = 0;
	var y = 0;
	for (var i = 0; i < data.length; i+=4) {
		if ((data[i] != data[i+1] || data[i+1] != data[i+2]) || (data[i] > 34) || (data[i] < 200)) { // (not gray at all) || (given gray, not white-ish) || (given gray, not black-ish)
			var rgb = data[i] + " " + data[i+1] + " " + data[i+2];
			if (rgb in new_color_map) { // Check if in keys
				new_context.fillStyle = new_color_map[rgb];
				new_context.fillRect(x, y, 1, 1);
			} 
		}
		x++;
		if (x == image_to_process.width) {
			x = 0;
			y++;
		}
	}
	
	// Turn canvas into image and replace original image with canvas
	var new_image = new Image();
	new_image.src = new_canvas.toDataURL();
	new_image.setAttribute('cbmf-original-url', image_to_process.getAttribute("cbmf-original-url")); // original url tracking for cbmfRestoreImage
	image_to_process.parentNode.replaceChild(new_image, image_to_process);
	
	// Clean up
	delete new_canvas;
	delete image_to_process;
}

// Compares similar colors to deal with anti-aliasing, jpeg artifacts, etc.
function cbmfSimilarColors(a, b) {
	a_ar = a.split(" ");
	b_ar = b.split(" ");
	if (Math.abs(a_ar[0] - b_ar[0]) < 10 && Math.abs(a_ar[1] - b_ar[1]) < 10 && Math.abs(a_ar[2] - b_ar[2]) < 10) {
		return true;
	} else {
		return false;
	}
}

// Restore original image  as tracked, otherwise informs user already restored
function cbmfRestoreImage(image_to_process) {
	if (image_to_process.hasAttribute('cbmf-original-url')) {
		image_to_process.src = image_to_process.getAttribute("cbmf-original-url");
		image_to_process.removeAttribute("cbmf-original-url");
	} else {
		alert("Color-Blind Map Fixer:\nAlready at original image!");
	}
}