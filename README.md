====Color-Blind Map Fixer====


===General Summary===

Allows users to right click on maps and charts to replace colors with a color-blind friendly color palette. You can also click on completely random images and see what happens!

Color palette borrowed and modified from this image as of April 2019:
http://mkweb.bcgsc.ca/biovis2012/color-blindness-palette.png


===Context Menu Options===

.:Fix Colors:.
Takes the colors from charts and maps with distinct categories and re-colors them with color-blind friendly colors. 

Takes the 14 most common colors (excluding white, black, grays in-between) in an image and swaps them with color-blind friendly colors from the color palette.

Very similar looking colors will be merged together (such as #FF000 and #FA000). This is to address subtle gradients (such as a map projected onto a globe with shadows) and to eliminate JPEG artifacts (which are surprisingly common in online charts that should be PNGs). However for performance reasons, only the most common 1000 colors will ever be looked at for the purposes of merging. Note that edges near outlines may be a bit rough.

.:Restore Image:.
Returns image to original source. Note that sometimes websites do weird things with HTML, so on rare occasion, the returned image may be of the wrong size. 


===Justification For Permission Requests===

.:Menus:.
This one is pretty straightforward. The add-on is run from your context menu when you right-click on an image.

.:All URLs:.
This one is less straightforward, but it gives permission for the browser to read and process images whose sources are not of the same as the domain of the website you are currently visiting. An example may be right clicking on an image in a search result or an image that shows up embedded in a news aggregator. Read https://en.wikipedia.org/wiki/Cross-origin_resource_sharing for more information.


===Coming soon???===

Better handling of (or a separate option for) images with color continuums.


===Prologue===

My name is Chance Miller. I am not color-blind, but my friend is.

Like this utility? Check out some of my other shit at http://www.middleendian.com or send me some feedback at ch@ncemiller.com